---
title: Zork Chromebooks
---

`zork` is a board name for x86_64-based Chromebooks. Many vendors make
Chromebooks based on this board, some examples:

-   [Lenovo 300e Chromebook Gen 3 AMD](https://www.lenovo.com/us/en/coming-soon/300e-AMD-G3/p/22ED03E3EA3)
-   [HP Pro c645 Chromebook Enterprise](https://www8.hp.com/h20195/v2/GetDocument.aspx?docname=4AA7-9044ENUC)
-   [Acer Chromebook Spin 514](https://www.acer.com/ac/en/US/content/series/acerchromebookspin514)
-   [ASUS Chromebook Flip CM5](https://www.asus.com/Laptops/For-Home/Chromebook/ASUS-Chromebook-Flip-CM5-CM5500/)

These Chromebooks use 64 bit AMD microprocessors of the AMD APU series, such as Picasso, Dali and Pollock. The rest of the specs may vary between vendors and models.

The Collabora LAVA lab contains the following `zork` devices:

-   [Lenovo ThinkPad C13 Yoga Chromebook](https://www.lenovo.com/us/en/p/laptops/thinkpad/thinkpadc/thinkpad-c13-yoga-chromebook-enterprise/22tpc13c3y1) (codename `morphius`)
    -   See [lenovo-TPad-C13-Yoga-zork](https://lava.collabora.dev/scheduler/device_type/lenovo-TPad-C13-Yoga-zork) in LAVA
    -   CPU: AMD Ryzen 3 3250C
    -   GPU: AMD Picasso
    -   Arch: x86_64
    -   AUE Date: 2031-06
-   [HP x360 14a](https://support.hp.com/th-en/document/c07729525) (codename `gumboz`)
    -   Label: `TLA, NOTEBOOK, CHROMEBOOK, GUMBOZ, PVT, US, SKU1, AMD 3015Ce, 4GB RAM, 32GB EMMC, 14IN, HD, WIFI, BT`
    -   See [hp-x360-14a-cb0001xx-zork](https://lava.collabora.dev/scheduler/device_type/hp-x360-14a-cb0001xx-zork) in LAVA
    -   CPU: AMD 3015Ce
    -   GPU: AMD Picasso
    -   Aarch: x86_64
    -   AUE Date: 2031-06
-   HP 14b na0052xx (codename `berknip`)
    -   Label: `TLA, NOTEBOOK, CHROMEBOOK, BERKNIP, PVT, UK, SKUKV2, AMD RYZEN 3 3250C, 8GB RAM HYNIX, 64GB EMMC SANDISK, 14IN, FHD, WIFI, BT`
    -   See [hp-14b-na0052xx-zork](https://lava.collabora.dev/scheduler/device_type/hp-14b-na0052xx-zork) in LAVA
    -   CPU: AMD Ryzen 3 3250C
    -   GPU: AMD Picasso
    -   Aarch: x86_64
    -   AUE Date: 2031-06


### Debugging interfaces

`zork` boards have been flashed and tested with [SuzyQ](../../01-debugging_interfaces) cables.

In the Lenovo ThinkPad C13 Yoga Chromebook, the debug port is the USB-C port on the left side.

#### Network connectivity

In the lab, these Chromebooks are connected to the network through a
Techrise USB-Eth adapter (R8152-based).

#### Known issues

See [Common issues](../common_issues/).

##### Zork-3 Fails with ServoV4

Kernel panic after starting DHCP:

    [    9.337182] IPv6: ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready
    [    9.348420] Sending DHCP requests ......
    [   75.816414] ------------[ cut here ]------------
    [   75.824954] NETDEV WATCHDOG: eth0 (r8152): transmit queue 0 timed out
    [   75.831399] WARNING: CPU: 3 PID: 0 at net/sched/sch_generic.c:443 dev_watchdog+0x244/0x250
    [   75.839659] Modules linked in:
    [   75.842714] CPU: 3 PID: 0 Comm: swapper/3 Not tainted 5.12.0 #1
    [   75.848629] Hardware name: LENOVO Morphius/Morphius, BIOS Google_Morphius.13434.60.0 10/08/2020
    [   75.857324] RIP: 0010:dev_watchdog+0x244/0x250
    [   75.861766] Code: 41 5f 5d c3 c6 05 31 87 dc 00 01 4c 89 ff e8 23 3a fc ff 48 c7 c7 d7 d9 a7 9d 4c 89 fe 48 89 c2 44 89 f1 31 c0 e8 5c 82 5f ff <0f> 0b e9 25 ff ff ff 0f 1f 44 00 00 41 57 41 56 53 49 89 fe 83 bf
    [   75.880506] RSP: 0018:ffff970840128e80 EFLAGS: 00010246
    [   75.885729] RAX: 135e776430966400 RBX: 00000000000001c0 RCX: 135e776430966400
    [   75.892857] RDX: 00000000ffffdfff RSI: c0000000ffffdfff RDI: 0000000000001fff
    [   75.899983] RBP: ffff8abe4236f000 R08: ffffffff9dc7f590 R09: 0000000000000000
    [   75.907112] R10: 0000000000000000 R11: ffff970840128c88 R12: 0000000000000001
    [   75.914241] R13: dead000000000122 R14: 0000000000000000 R15: ffff8abe42331000
    [   75.921370] FS:  0000000000000000(0000) GS:ffff8abe6af80000(0000) knlGS:0000000000000000
    [   75.929451] CS:  0010 DS: 0000 ES: 0000 CR0: 0000000080050033
    [   75.935191] CR2: 0000000000000000 CR3: 000000009680c000 CR4: 00000000003506e0
    [   75.942321] Call Trace:
    [   75.944770]  <IRQ>
    [   75.946786]  ? dev_init_scheduler+0x90/0x90
    [   75.950969]  call_timer_fn+0x55/0xf0
    [   75.954546]  expire_timers+0x3c/0xf0
    [   75.958121]  __run_timers+0x11f/0x160
    [   75.961783]  ? clockevents_switch_state+0x11/0x40
    [   75.966487]  ? tick_program_event+0x4e/0x70
    [   75.970669]  ? hrtimer_interrupt+0x2f7/0x420
    [   75.974934]  run_timer_softirq+0x14/0x30
    [   75.978858]  __do_softirq+0x15f/0x26d
    [   75.982521]  __irq_exit_rcu+0xa5/0xb0
    [   75.986184]  sysvec_apic_timer_interrupt+0x68/0x80
    [   75.990975]  </IRQ>
    [   75.993077]  asm_sysvec_apic_timer_interrupt+0x12/0x20
    [   75.998211] RIP: 0010:cpuidle_enter_state+0x1de/0x2d0
    [   76.003257] Code: 31 ff e8 d5 0f 71 ff 45 84 ed 74 1a 9c 8f 44 24 08 f7 44 24 08 00 02 00 00 0f 85 e6 00 00 00 31 ff e8 06 14 77 ff fb 45 85 ff <78> 51 44 89 f8 48 6b d0 68 48 89 de 48 8b 4c 13 48 48 2b 2c 24 49
    [   76.022001] RSP: 0018:ffff97084009be90 EFLAGS: 00000202
    [   76.027223] RAX: ffff8abe6afa9240 RBX: ffffffff9ddac110 RCX: 000000000000001f
    [   76.034350] RDX: 0000000000000003 RSI: 00000011a91cfcad RDI: 0000000000000000
    [   76.041480] RBP: 00000011a7029500 R08: 0000000000000004 R09: 000000003152be0b
    [   76.048607] R10: 0000000000078f23 R11: ffffffff9cb30f90 R12: 0000000000000001
    [   76.055736] R13: 0000000000000000 R14: ffff8abe41fd5400 R15: 0000000000000001
    [   76.062862]  ? acpi_idle_lpi_enter+0x30/0x30
    [   76.067133]  cpuidle_enter+0x24/0x40
    [   76.070708]  do_idle+0x1a3/0x240
    [   76.073938]  cpu_startup_entry+0x15/0x20
    [   76.077858]  secondary_startup_64_no_verify+0xc2/0xcb
    [   76.082908] ---[ end trace 3cc9f5bfb34d12bf ]---

which causes future tests to fail with the following error:

    tftpboot 192.168.201.1 5483964/tftp-deploy-o2ecj82w/kernel/bzImage 5w/kernel/cmdline 5483964/tftp-deploy-o2ecj82w/ramdisk/ramdisk.cpio.gz
    Waiting for link
    auto-login-action timed out after 120 seconds

The workaround we're using is to use a SuzyQ cable instead.

### Example kernel command line arguments

    console=ttyS0,115200n8 root=/dev/nfs rw nfsroot=10.154.97.199:/srv/nfs/chromebook,tcp,hard ip=dhcp tftpserverip=10.154.97.199

The IP and path of the NFS share are examples and should be adapted to
the test setup.

### Firmware

Firmware flashed on all `morphius` units in the lab (based on the ChromeOS `morphius` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/zork-morphius.bin>

Firmware flashed on all `gumboz` units in the lab (based on the ChromeOS `gumboz` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/zork-gumboz.bin>

Firmware flashed on all `berknip` units in the lab (based on the ChromeOS `berknip` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/zork-berknip.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.

### Hardware info

Output of `lshw` and `lspci` commands for the specific device types in the Collabora LAVA lab:

-   `lenovo-TPad-C13-Yoga-zork`:

    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    H/W path              Device  Class          Description
    ========================================================
    /0/0                          memory         1MiB BIOS
    /0/4                          processor      AMD Ryzen 3 3250C 15W with Radeon G
    /0/4/6                        memory         64KiB L1 cache
    /0/4/7                        memory         512KiB L2 cache
    /0/4/8                        memory         4MiB L3 cache
    /0/5                          memory         32KiB L1 cache
    /0/a                          memory         4GiB System Memory
    /0/a/0                        memory         4GiB SODIMM DDR4 Synchronous 3200 M
    /0/100                        bridge         Raven/Raven2 Root Complex
    /0/100/1.2                    bridge         Raven/Raven2 PCIe GPP Bridge [6:0]
    /0/100/1.2/0                  network        Wi-Fi 6 AX200
    /0/100/1.3                    bridge         Raven/Raven2 PCIe GPP Bridge [6:0]
    /0/100/1.7                    bridge         Raven/Raven2 PCIe GPP Bridge [6:0]
    /0/100/8.1                    bridge         Raven/Raven2 Internal PCIe GPP Brid
    /0/100/8.1/0                  display        Picasso
    /0/100/14.3                   bridge         FCH LPC Bridge
    /0/101                        bridge         Family 17h (Models 00h-1fh) PCIe Du
    /0/102                        bridge         Family 17h (Models 00h-1fh) PCIe Du
    /0/103                        bridge         Raven/Raven2 Device 24: Function 0
    /0/104                        bridge         Raven/Raven2 Device 24: Function 1
    /0/105                        bridge         Raven/Raven2 Device 24: Function 2
    /0/106                        bridge         Raven/Raven2 Device 24: Function 3
    /0/107                        bridge         Raven/Raven2 Device 24: Function 4
    /0/108                        bridge         Raven/Raven2 Device 24: Function 5
    /0/109                        bridge         Raven/Raven2 Device 24: Function 6
    /0/10a                        bridge         Raven/Raven2 Device 24: Function 7
    /1                    eth0    network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Root Complex
    00:00.2 IOMMU: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 IOMMU
    00:01.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Family 17h (Models 00h-1fh) PCIe Dummy Host Bridge
    00:01.2 PCI bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 PCIe GPP Bridge [6:0]
    00:01.3 PCI bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 PCIe GPP Bridge [6:0]
    00:01.7 PCI bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 PCIe GPP Bridge [6:0]
    00:08.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Family 17h (Models 00h-1fh) PCIe Dummy Host Bridge
    00:08.1 PCI bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Internal PCIe GPP Bridge 0 to Bus A
    00:14.0 SMBus: Advanced Micro Devices, Inc. [AMD] FCH SMBus Controller (rev 61)
    00:14.3 ISA bridge: Advanced Micro Devices, Inc. [AMD] FCH LPC Bridge (rev 51)
    00:18.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 0
    00:18.1 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 1
    00:18.2 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 2
    00:18.3 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 3
    00:18.4 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 4
    00:18.5 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 5
    00:18.6 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 6
    00:18.7 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 7
    01:00.0 Network controller: Intel Corporation Wi-Fi 6 AX200 (rev 1a)
    02:00.0 SD Host controller: Genesys Logic, Inc GL9750 SD Host Controller (rev 01)
    03:00.0 Non-Volatile memory controller: KIOXIA Corporation Device 0001
    04:00.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Picasso (rev c4)
    04:00.1 Audio device: Advanced Micro Devices, Inc. [AMD/ATI] Raven/Raven2/Fenghuang HDMI/DP Audio Controller
    04:00.2 Encryption controller: Advanced Micro Devices, Inc. [AMD] Family 17h (Models 10h-1fh) Platform Security Processor
    04:00.3 USB controller: Advanced Micro Devices, Inc. [AMD] Raven2 USB 3.1
    04:00.5 Multimedia controller: Advanced Micro Devices, Inc. [AMD] Raven/Raven2/FireFlight/Renoir Audio Processor
    04:00.7 Non-VGA unclassified device: Advanced Micro Devices, Inc. [AMD] Raven/Raven2/Renoir Sensor Fusion Hub
    ```

-   `hp-x360-14a-cb0001xx-zork`:
    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
        H/W path            Device  Class          Description
    ======================================================
    /0/0                        memory         1MiB BIOS
    /0/4                        processor      AMD 3015Ce with Radeon Graphics
    /0/4/6                      memory         64KiB L1 cache
    /0/4/7                      memory         512KiB L2 cache
    /0/4/8                      memory         4MiB L3 cache
    /0/5                        memory         32KiB L1 cache
    /0/a                        memory         4GiB System Memory
    /0/a/0                      memory         4GiB SODIMM DDR4 Synchronous 2666 MHz
    /0/100                      bridge         Raven/Raven2 Root Complex
    /0/100/1.2                  bridge         Raven/Raven2 PCIe GPP Bridge [6:0]
    /0/100/1.2/0                network        RTL8822CE 802.11ac PCIe Wireless Netw
    /0/100/8.1                  bridge         Raven/Raven2 Internal PCIe GPP Bridge
    /0/100/8.1/0                display        Picasso
    /0/100/14.3                 bridge         FCH LPC Bridge
    /0/101                      bridge         Family 17h (Models 00h-1fh) PCIe Dumm
    /0/102                      bridge         Family 17h (Models 00h-1fh) PCIe Dumm
    /0/103                      bridge         Raven/Raven2 Device 24: Function 0
    /0/104                      bridge         Raven/Raven2 Device 24: Function 1
    /0/105                      bridge         Raven/Raven2 Device 24: Function 2
    /0/106                      bridge         Raven/Raven2 Device 24: Function 3
    /0/107                      bridge         Raven/Raven2 Device 24: Function 4
    /0/108                      bridge         Raven/Raven2 Device 24: Function 5
    /0/109                      bridge         Raven/Raven2 Device 24: Function 6
    /0/10a                      bridge         Raven/Raven2 Device 24: Function 7
    /1                  eth0    network        Ethernet interface
    ```
    ```console
    lspci
    00:00.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Root Complex
    00:00.2 IOMMU: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 IOMMU
    00:01.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Family 17h (Models 00h-1fh) PCIe Dummy Host Bridge
    00:01.2 PCI bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 PCIe GPP Bridge [6:0]
    00:08.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Family 17h (Models 00h-1fh) PCIe Dummy Host Bridge
    00:08.1 PCI bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Internal PCIe GPP Bridge 0 to Bus A
    00:14.0 SMBus: Advanced Micro Devices, Inc. [AMD] FCH SMBus Controller (rev 61)
    00:14.3 ISA bridge: Advanced Micro Devices, Inc. [AMD] FCH LPC Bridge (rev 51)
    00:18.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 0
    00:18.1 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 1
    00:18.2 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 2
    00:18.3 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 3
    00:18.4 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 4
    00:18.5 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 5
    00:18.6 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 6
    00:18.7 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 7
    01:00.0 Network controller: Realtek Semiconductor Co., Ltd. RTL8822CE 802.11ac PCIe Wireless Network Adapter
    02:00.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Picasso (rev ea)
    02:00.1 Audio device: Advanced Micro Devices, Inc. [AMD/ATI] Raven/Raven2/Fenghuang HDMI/DP Audio Controller
    02:00.2 Encryption controller: Advanced Micro Devices, Inc. [AMD] Family 17h (Models 10h-1fh) Platform Security Processor
    02:00.3 USB controller: Advanced Micro Devices, Inc. [AMD] Raven2 USB 3.1
    02:00.5 Multimedia controller: Advanced Micro Devices, Inc. [AMD] Raven/Raven2/FireFlight/Renoir Audio Processor
    02:00.7 Non-VGA unclassified device: Advanced Micro Devices, Inc. [AMD] Raven/Raven2/Renoir Sensor Fusion Hub
    ```

-   `hp-14b-na0052xx-zork`:
    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    Device                      Class          Description
    ======================================================
    /0/0                        memory         1MiB BIOS
    /0/4                        processor      AMD Ryzen 3 3250C 15W with Radeon Gra
    /0/4/6                      memory         64KiB L1 cache
    /0/4/7                      memory         512KiB L2 cache
    /0/4/8                      memory         4MiB L3 cache
    /0/5                        memory         32KiB L1 cache
    /0/a                        memory         8GiB System Memory
    /0/a/0                      memory         4GiB SODIMM DDR4 Synchronous 2666 MHz
    /0/a/1                      memory         4GiB SODIMM DDR4 Synchronous 2666 MHz
    /0/100                      bridge         Raven/Raven2 Root Complex
    /0/100/1.2                  bridge         Raven/Raven2 PCIe GPP Bridge [6:0]
    /0/100/1.2/0                network        Wi-Fi 6 AX200
    /0/100/1.3                  bridge         Raven/Raven2 PCIe GPP Bridge [6:0]
    /0/100/8.1                  bridge         Raven/Raven2 Internal PCIe GPP Bridge
    /0/100/8.1/0                display        Picasso
    /0/100/14.3                 bridge         FCH LPC Bridge
    /0/101                      bridge         Family 17h (Models 00h-1fh) PCIe Dumm
    /0/102                      bridge         Family 17h (Models 00h-1fh) PCIe Dumm
    /0/103                      bridge         Raven/Raven2 Device 24: Function 0
    /0/104                      bridge         Raven/Raven2 Device 24: Function 1
    /0/105                      bridge         Raven/Raven2 Device 24: Function 2
    /0/106                      bridge         Raven/Raven2 Device 24: Function 3
    /0/107                      bridge         Raven/Raven2 Device 24: Function 4
    /0/108                      bridge         Raven/Raven2 Device 24: Function 5
    /0/109                      bridge         Raven/Raven2 Device 24: Function 6
    /0/10a                      bridge         Raven/Raven2 Device 24: Function 7
    /1                  eth0    network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Root Complex
    00:00.2 IOMMU: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 IOMMU
    00:01.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Family 17h (Models 00h-1fh) PCIe Dummy Host Bridge
    00:01.2 PCI bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 PCIe GPP Bridge [6:0]
    00:01.3 PCI bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 PCIe GPP Bridge [6:0]
    00:08.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Family 17h (Models 00h-1fh) PCIe Dummy Host Bridge
    00:08.1 PCI bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Internal PCIe GPP Bridge 0 to Bus A
    00:14.0 SMBus: Advanced Micro Devices, Inc. [AMD] FCH SMBus Controller (rev 61)
    00:14.3 ISA bridge: Advanced Micro Devices, Inc. [AMD] FCH LPC Bridge (rev 51)
    00:18.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 0
    00:18.1 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 1
    00:18.2 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 2
    00:18.3 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 3
    00:18.4 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 4
    00:18.5 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 5
    00:18.6 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 6
    00:18.7 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 7
    01:00.0 Network controller: Intel Corporation Wi-Fi 6 AX200 (rev 1a)
    02:00.0 SD Host controller: Genesys Logic, Inc GL9750 SD Host Controller (rev 01)
    03:00.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Picasso (rev c4)
    03:00.1 Audio device: Advanced Micro Devices, Inc. [AMD/ATI] Raven/Raven2/Fenghuang HDMI/DP Audio Controller
    03:00.2 Encryption controller: Advanced Micro Devices, Inc. [AMD] Family 17h (Models **10h**-1fh) Platform Security Processor
    03:00.3 USB controller: Advanced Micro Devices, Inc. [AMD] Raven2 USB 3.1
    03:00.5 Multimedia controller: Advanced Micro Devices, Inc. [AMD] Raven/Raven2/FireFlight/Renoir Audio Processor
    03:00.7 Non-VGA unclassified device: Advanced Micro Devices, Inc. [AMD] Raven/Raven2/Renoir Sensor Fusion Hub
    ```