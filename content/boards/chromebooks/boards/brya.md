---
title: Brya Chromebooks
---

`brya` is a board name for x86_64-based Chromebooks.

The Collabora LAVA lab contains the following `brya` devices:

-   [ASUS Chromebook Vero 514 CBV514-1H](https://www.acer.com/us-en/chromebooks/acer_chromebook_vero_514_cbv514-1h_cbv514-1ht/pdp/NX.KAJAA.001) (codename `volmar`)
    -   Label: `TLA, NOTEBOOK, CHROMEBOOK, VOLMAR, DVT, US, SKU2, INTEL CORE I3-1215U, 8GB RAM, 256GB, 14IN, FHD, WIFI, BT, USED`
    -   See [acer-cbv514-1h-34uz-brya](https://lava.collabora.dev/scheduler/device_type/acer-cbv514-1h-34uz-brya) in LAVA
    -   CPU: Intel Core i3-1215U
    -   GPU: Intel UHD Graphics
    -   Arch: x86_64
    -   AUE Date: 2023-06

### Debugging interfaces

`brya` boards have been flashed and tested with [SuzyQ](../../01-debugging_interfaces) cables.

In an ASUS Chromebook Vero 514, the debug port is the USB-C port on the left side.

`brya` requires a fairly recent version of [hdctools](https://chromium.googlesource.com/chromiumos/third_party/hdctools/) for debugging, as the servod overlay for this board was added in March 2022 (see: https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/94b20c1d34de6745e6ecd16174e14e56b896304a ).

In the Collabora lab, we use [docker-servod-tools](https://gitlab.collabora.com/lava/docker-servod-tools) to handle `brya` with the required servod version.

#### Network connectivity

In the lab, these Chromebooks are connected to the network through a
Techrise USB-Eth adapter (R8152-based).

#### Known issues

See [Common issues](../common_issues/).

### Example kernel command line arguments

    console=ttyS0,115200n8 root=/dev/nfs rw nfsroot=10.154.97.199:/srv/nfs/chromebook,tcp,hard ip=dhcp tftpserverip=10.154.97.199

The IP and path of the NFS share are examples and should be adapted to
the test setup.

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `volmar` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/brya.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.

### Hardware info

Output of `lshw` and `lspci` commands for the specific device types in the Collabora LAVA lab:

-   `acer-cbv514-1h-34uz-brya`:

    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    H/W path       Device  Class          Description
    =================================================
    /0/0                   memory         1MiB BIOS
    /0/4                   processor      12th Gen Intel(R) Core(TM) i3-1215U
    /0/4/6                 memory         128KiB L1 cache
    /0/4/7                 memory         1280KiB L2 cache
    /0/4/8                 memory         10MiB L3 cache
    /0/5                   memory         192KiB L1 cache
    /0/b                   memory         8GiB System Memory
    /0/b/0                 memory         1GiB Row of chips LPDDR4 Synchronous 4267
    /0/b/1                 memory         1GiB Row of chips LPDDR4 Synchronous 4267
    /0/b/2                 memory         1GiB Row of chips LPDDR4 Synchronous 4267
    /0/b/3                 memory         1GiB Row of chips LPDDR4 Synchronous 4267
    /0/b/4                 memory         1GiB Row of chips LPDDR4 Synchronous 4267
    /0/b/5                 memory         1GiB Row of chips LPDDR4 Synchronous 4267
    /0/b/6                 memory         1GiB Row of chips LPDDR4 Synchronous 4267
    /0/b/7                 memory         1GiB Row of chips LPDDR4 Synchronous 4267
    /0/100                 bridge         Intel Corporation
    /0/100/2               display        Intel Corporation
    /0/100/6               bridge         Intel Corporation
    /0/100/14.2            memory         RAM memory
    /0/100/14.3            network        Intel Corporation
    /0/100/1f              bridge         Intel Corporation
    /1             eth0    network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Intel Corporation Device 4609 (rev 04)
    00:02.0 VGA compatible controller: Intel Corporation Device 46b3 (rev 0c)
    00:04.0 Signal processing controller: Intel Corporation Device 461d (rev 04)
    00:06.0 PCI bridge: Intel Corporation Device 464d (rev 04)
    00:08.0 System peripheral: Intel Corporation Device 464f (rev 04)
    00:0a.0 Signal processing controller: Intel Corporation Device 467d (rev 01)
    00:0d.0 USB controller: Intel Corporation Device 461e (rev 04)
    00:14.0 USB controller: Intel Corporation Device 51ed (rev 01)
    00:14.2 RAM memory: Intel Corporation Device 51ef (rev 01)
    00:14.3 Network controller: Intel Corporation Device 51f0 (rev 01)
    00:15.0 Serial bus controller [0c80]: Intel Corporation Device 51e8 (rev 01)
    00:15.1 Serial bus controller [0c80]: Intel Corporation Device 51e9 (rev 01)
    00:15.2 Serial bus controller [0c80]: Intel Corporation Device 51ea (rev 01)
    00:15.3 Serial bus controller [0c80]: Intel Corporation Device 51eb (rev 01)
    00:16.0 Communication controller: Intel Corporation Device 51e0 (rev 01)
    00:19.0 Serial bus controller [0c80]: Intel Corporation Device 51c5 (rev 01)
    00:19.1 Serial bus controller [0c80]: Intel Corporation Device 51c6 (rev 01)
    00:1e.0 Communication controller: Intel Corporation Device 51a8 (rev 01)
    00:1e.3 Serial bus controller [0c80]: Intel Corporation Device 51ab (rev 01)
    00:1f.0 ISA bridge: Intel Corporation Device 5182 (rev 01)
    00:1f.3 Multimedia audio controller: Intel Corporation Device 51c8 (rev 01)
    00:1f.5 Serial bus controller [0c80]: Intel Corporation Device 51a4 (rev 01)
    01:00.0 Non-Volatile memory controller: Sandisk Corp WD Blue SN550 NVMe SSD (rev 01)
    ```
