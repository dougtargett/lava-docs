---
title: Hatch Chromebooks
---

`hatch` is a board name for x86-64 Chromebooks using an Intel Core i5-10210U.

The Collabora LAVA lab contains the following `hatch` devices:

-   [ASUS Chromebook Flip C436FA](https://www.asus.com/2-in-1-PCs/ASUS-Chromebook-Flip-C436FA/) (codename `helios`)
    -   See [asus-C436FA-Flip-hatch](https://lava.collabora.dev/scheduler/device_type/asus-C436FA-Flip-hatch) in LAVA
    -   CPU: Intel Core i5-10210U CPU @ 1.60
    -   GPU: CometLake-U GT2 [UHD Graphics]
    -   Arch: x86_64
    -   AUE Date: 2030-06

### Debugging interfaces

`hatch` boards have been flashed and tested with [Servo
v4](../../01-debugging_interfaces) interfaces.

The Asus Chromebook Flip C436FA supports CCD and can be debugged through
its USB-C port on the left side (power supply's one).

#### Network connectivity

The Servo v4 interface includes an Ethernet interface with a chipset
supported by Depthcharge (R8152).

#### Known issues

No specific issues found for this Chromebook yet, but see [Common
issues](../common_issues/).

### Example kernel command line arguments

An example of kernel command line arguments to boot a FIT image:

    earlyprintk=uart8250,mmio32,0xde000000,115200n8 console=ttyS0,115200n8 root=/dev/nfs ip=dhcp rootwait rw nfsroot=192.168.2.100:/srv/nfs/chromebook,v3 nfsrootdebug

The IP and path of the NFS share are examples and should be adapted to
the test setup.

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `helios` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/hatch.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.

### Hardware info

Output of `lshw` and `lspci` commands for the specific device types in the Collabora LAVA lab:

-   `asus-C436FA-Flip-hatch`:

    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    H/W path         Device    Class          Description
    =====================================================
    /0/0                       memory         1MiB BIOS
    /0/4                       processor      CPU [empty]
    /0/4/6                     memory         32KiB L1 cache
    /0/4/7                     memory         256KiB L2 cache
    /0/4/8                     memory         6MiB L3 cache
    /0/5                       memory         32KiB L1 cache
    /0/1                       memory         16GiB System memory
    /0/1/0                     memory         8GiB LPDDR3 Synchronous 2133 MHz (0.5
    /0/1/1                     memory         8GiB LPDDR3 Synchronous 2133 MHz (0.5
    /0/2                       processor      Intel(R) Core(TM) i5-10210U CPU @ 1.60
    /0/100                     bridge         Comet Lake-U v1 4c Host Bridge/DRAM Co
    /0/100/2                   display        CometLake-U GT2 [UHD Graphics]
    /0/100/14.2                memory         RAM memory
    /0/100/14.3                network        Comet Lake PCH-LP CNVi WiFi
    /0/100/1d                  bridge         Intel Corporation
    /0/100/1f                  bridge         Comet Lake PCH-LP LPC Premium Controll
    /1               eth0      network        Ethernet interface
    /2               eth1      network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Intel Corporation Comet Lake-U v1 4c Host Bridge/DRAM Controller (rev 0c)
    00:02.0 VGA compatible controller: Intel Corporation CometLake-U GT2 [UHD Graphics] (rev 02)
    00:04.0 Signal processing controller: Intel Corporation Xeon E3-1200 v5/E3-1500 v5/6th Gen Core Processor Thermal Subsystem (rev 0c)
    00:08.0 System peripheral: Intel Corporation Xeon E3-1200 v5/v6 / E3-1500 v5 / 6th/7th/8th Gen Core Processor Gaussian Mixture Model
    00:12.0 Signal processing controller: Intel Corporation Comet Lake Thermal Subsytem
    00:14.0 USB controller: Intel Corporation Comet Lake PCH-LP USB 3.1 xHCI Host Controller
    00:14.2 RAM memory: Intel Corporation Comet Lake PCH-LP Shared SRAM
    00:14.3 Network controller: Intel Corporation Comet Lake PCH-LP CNVi WiFi
    00:15.0 Serial bus controller [0c80]: Intel Corporation Serial IO I2C Host Controller
    00:15.1 Serial bus controller [0c80]: Intel Corporation Comet Lake Serial IO I2C Host Controller
    00:17.0 SATA controller: Intel Corporation Comet Lake SATA AHCI Controller
    00:19.0 Serial bus controller [0c80]: Intel Corporation Comet Lake Serial IO I2C Host Controller
    00:1d.0 PCI bridge: Intel Corporation Device 02b0 (rev f0)
    00:1e.0 Communication controller: Intel Corporation Device 02a8
    00:1e.2 Serial bus controller [0c80]: Intel Corporation Device 02aa
    00:1e.3 Serial bus controller [0c80]: Intel Corporation Device 02ab
    00:1f.0 ISA bridge: Intel Corporation Comet Lake PCH-LP LPC Premium Controller/eSPI Controller
    00:1f.3 Multimedia audio controller: Intel Corporation Comet Lake PCH-LP cAVS
    00:1f.4 SMBus: Intel Corporation Comet Lake PCH-LP SMBus Host Controller
    00:1f.5 Serial bus controller [0c80]: Intel Corporation Comet Lake SPI (flash) Controller
    01:00.0 Non-Volatile memory controller: SK hynix BC501 NVMe Solid State Drive 512GB
    ```
