---
title: Volteer Chromebooks
---

`volteer` is a board name for x86_64-based Chromebooks based on the
TigerLake architecture.

The Collabora LAVA lab currently contains the following `volteer` devices:

-   [ASUS CX9400](https://www.asus.com/laptops/for-work/chromebook/asus-chromebook-cx9-cx9400-11th-gen-intel/techspec/) (codename `voema`, i5 variant)
    -   Label: `VOEMA DVT SKU 2 970-07297-01-R`
    -   See `cbg-0`,`cbg-1`,`cbg-2`,`cbg-3`,`cbg-4`,`cbg-5`,`cbg-6`,`cbg-7`,`cbg-8`,`cbg-9` of [asus-cx9400-volteer](https://lava.collabora.dev/scheduler/device_type/asus-cx9400-volteer) in LAVA
    -   CPU: 11th Gen Intel Core i5-1130G7
    -   GPU: Intel Iris Xe Graphics
    -   Arch: x86_64
    -   AUE Date: 2031-06

-   [ASUS CX9400](https://www.asus.com/laptops/for-work/chromebook/asus-chromebook-cx9-cx9400-11th-gen-intel/techspec/) (codename `voema`, i7 variant)
    -   Label: `TLA, NOTEBOOK, CHROMEBOOK, VOEMA, PVT, US, SKU2, INTEL CORE I7-1160G7, 16GB RAM, 512GB, TOUCH, 14IN, FHD, WIFI, BT`
    -   See `cbg-10`,`cbg-11`,`cbg-12`,`cbg-13`,`cbg-14` of [asus-cx9400-volteer](https://lava.collabora.dev/scheduler/device_type/asus-cx9400-volteer) in LAVA
    -   CPU: 11th Gen Intel Core i7-1160G7
    -   GPU: Intel Iris Xe Graphics
    -   Arch: x86_64
    -   AUE Date: 2031-06

_Note_: the codename reported for the ASUS CX9400 Chromebook in [Chromium Dash](https://chromiumdash.appspot.com/serving-builds?deviceCategory=Chrome%20OS) is `drobit`. However, the label on the units in the Collabora LAVA lab reports `voema` instead and the EC came pre-flashed with a `voema` firmware variant too.

### Debugging interfaces

`volteer` boards have been flashed and tested with both
[SuzyQ](../../01-debugging_interfaces) cables.

#### Network connectivity

In the lab, these Chromebooks are connected to the network through a
Techrise USB-Eth adapter (R8152-based).

#### Known issues

See [Common issues](../common_issues/).

### Example kernel command line arguments

    console=ttyS0,115200n8 root=/dev/nfs ip=dhcp tftpserverip=10.154.97.199 ip=dhcp rootwait rw nfsroot=192.168.2.100:/srv/nfs/chromebook,v3 nfsrootdebug

The IP and path of the NFS share are examples and should be adapted to
the test setup.

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `voema` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/volteer.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.

### Hardware info

Output of `lshw` and `lspci` commands for the specific device types in the Collabora LAVA lab:

-   `asus-cx9400-volteer` (i5 variant):

    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    H/W path       Device    Class          Description
    ===================================================
    /0/0                     memory         1MiB BIOS
    /0/4                     processor      11th Gen Intel(R) Core(TM) i5-1130G7 @ 1
    /0/4/6                   memory         128KiB L1 cache
    /0/4/7                   memory         5MiB L2 cache
    /0/4/8                   memory         8MiB L3 cache
    /0/5                     memory         192KiB L1 cache
    /0/b                     memory         8GiB System Memory
    /0/b/0                   memory         1GiB LPDDR4 Synchronous 4267 MHz (0.2 ns
    /0/b/1                   memory         1GiB LPDDR4 Synchronous 4267 MHz (0.2 ns
    /0/b/2                   memory         1GiB LPDDR4 Synchronous 4267 MHz (0.2 ns
    /0/b/3                   memory         1GiB LPDDR4 Synchronous 4267 MHz (0.2 ns
    /0/b/4                   memory         1GiB LPDDR4 Synchronous 4267 MHz (0.2 ns
    /0/b/5                   memory         1GiB LPDDR4 Synchronous 4267 MHz (0.2 ns
    /0/b/6                   memory         1GiB LPDDR4 Synchronous 4267 MHz (0.2 ns
    /0/b/7                   memory         1GiB LPDDR4 Synchronous 4267 MHz (0.2 ns
    /0/100                   bridge         Intel Corporation
    /0/100/2                 display        Intel Corporation
    /0/100/14.2              memory         RAM memory
    /0/100/14.3              network        Wi-Fi 6 AX201
    /0/100/1d                bridge         Tiger Lake-LP PCI Express Root Port #9
    /0/100/1f                bridge         Intel Corporation
    /1             eth0      network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Intel Corporation Device 9a12 (rev 01)
    00:02.0 VGA compatible controller: Intel Corporation Device 9a40 (rev 01)
    00:04.0 Signal processing controller: Intel Corporation Device 9a03 (rev 01)
    00:05.0 Multimedia controller: Intel Corporation Device 9a19 (rev 01)
    00:08.0 System peripheral: Intel Corporation Device 9a11 (rev 01)
    00:0a.0 Signal processing controller: Intel Corporation Device 9a0d (rev 01)
    00:0d.0 USB controller: Intel Corporation Tiger Lake-LP Thunderbolt 4 USB Controller (rev 01)
    00:14.0 USB controller: Intel Corporation Tiger Lake-LP USB 3.2 Gen 2x1 xHCI Host Controller (rev 20)
    00:14.2 RAM memory: Intel Corporation Tiger Lake-LP Shared SRAM (rev 20)
    00:14.3 Network controller: Intel Corporation Wi-Fi 6 AX201 (rev 20)
    00:15.0 Serial bus controller [0c80]: Intel Corporation Tiger Lake-LP Serial IO I2C Controller #0 (rev 20)
    00:15.1 Serial bus controller [0c80]: Intel Corporation Tiger Lake-LP Serial IO I2C Controller #1 (rev 20)
    00:15.2 Serial bus controller [0c80]: Intel Corporation Tiger Lake-LP Serial IO I2C Controller #2 (rev 20)
    00:15.3 Serial bus controller [0c80]: Intel Corporation Tiger Lake-LP Serial IO I2C Controller #3 (rev 20)
    00:19.0 Serial bus controller [0c80]: Intel Corporation Tiger Lake-LP Serial IO I2C Controller #4 (rev 20)
    00:19.1 Serial bus controller [0c80]: Intel Corporation Tiger Lake-LP Serial IO I2C Controller #5 (rev 20)
    00:1d.0 PCI bridge: Intel Corporation Tiger Lake-LP PCI Express Root Port #9 (rev 20)
    00:1e.0 Communication controller: Intel Corporation Tiger Lake-LP Serial IO UART Controller #0 (rev 20)
    00:1e.2 Serial bus controller [0c80]: Intel Corporation Device a0aa (rev 20)
    00:1e.3 Serial bus controller [0c80]: Intel Corporation Tiger Lake-LP Serial IO SPI Controller #1 (rev 20)
    00:1f.0 ISA bridge: Intel Corporation Device a087 (rev 20)
    00:1f.3 Multimedia audio controller: Intel Corporation Tiger Lake-LP Smart Sound Technology Audio Controller (rev 20)
    00:1f.5 Serial bus controller [0c80]: Intel Corporation Tiger Lake-LP SPI Controller (rev 20)
    01:00.0 Non-Volatile memory controller: SK hynix Device 174a
    ```

-   `asus-cx9400-volteer` (i7 variant):

    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    H/W path       Device    Class          Description
    ===================================================
    /0/0                     memory         1MiB BIOS
    /0/4                     processor      11th Gen Intel(R) Core(TM) i7-1160G7 @ 1
    /0/4/6                   memory         128KiB L1 cache
    /0/4/7                   memory         5MiB L2 cache
    /0/4/8                   memory         12MiB L3 cache
    /0/5                     memory         192KiB L1 cache
    /0/b                     memory         16GiB System Memory
    /0/b/0                   memory         2GiB LPDDR4 Synchronous 4267 MHz (0.2 ns
    /0/b/1                   memory         2GiB LPDDR4 Synchronous 4267 MHz (0.2 ns
    /0/b/2                   memory         2GiB LPDDR4 Synchronous 4267 MHz (0.2 ns
    /0/b/3                   memory         2GiB LPDDR4 Synchronous 4267 MHz (0.2 ns
    /0/b/4                   memory         2GiB LPDDR4 Synchronous 4267 MHz (0.2 ns
    /0/b/5                   memory         2GiB LPDDR4 Synchronous 4267 MHz (0.2 ns
    /0/b/6                   memory         2GiB LPDDR4 Synchronous 4267 MHz (0.2 ns
    /0/b/7                   memory         2GiB LPDDR4 Synchronous 4267 MHz (0.2 ns
    /0/100                   bridge         Intel Corporation
    /0/100/2                 display        Intel Corporation
    /0/100/14.2              memory         RAM memory
    /0/100/14.3              network        Wi-Fi 6 AX201
    /0/100/1d                bridge         Tiger Lake-LP PCI Express Root Port #9
    /0/100/1f                bridge         Intel Corporation
    /1             eth0      network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Intel Corporation Device 9a12 (rev 01)
    00:02.0 VGA compatible controller: Intel Corporation Device 9a40 (rev 01)
    00:04.0 Signal processing controller: Intel Corporation Device 9a03 (rev 01)
    00:05.0 Multimedia controller: Intel Corporation Device 9a19 (rev 01)
    00:08.0 System peripheral: Intel Corporation Device 9a11 (rev 01)
    00:0a.0 Signal processing controller: Intel Corporation Device 9a0d (rev 01)
    00:0d.0 USB controller: Intel Corporation Tiger Lake-LP Thunderbolt 4 USB Controller (rev 01)
    00:14.0 USB controller: Intel Corporation Tiger Lake-LP USB 3.2 Gen 2x1 xHCI Host Controller (rev 20)
    00:14.2 RAM memory: Intel Corporation Tiger Lake-LP Shared SRAM (rev 20)
    00:14.3 Network controller: Intel Corporation Wi-Fi 6 AX201 (rev 20)
    00:15.0 Serial bus controller [0c80]: Intel Corporation Tiger Lake-LP Serial IO I2C Controller #0 (rev 20)
    00:15.1 Serial bus controller [0c80]: Intel Corporation Tiger Lake-LP Serial IO I2C Controller #1 (rev 20)
    00:15.2 Serial bus controller [0c80]: Intel Corporation Tiger Lake-LP Serial IO I2C Controller #2 (rev 20)
    00:15.3 Serial bus controller [0c80]: Intel Corporation Tiger Lake-LP Serial IO I2C Controller #3 (rev 20)
    00:19.0 Serial bus controller [0c80]: Intel Corporation Tiger Lake-LP Serial IO I2C Controller #4 (rev 20)
    00:19.1 Serial bus controller [0c80]: Intel Corporation Tiger Lake-LP Serial IO I2C Controller #5 (rev 20)
    00:1d.0 PCI bridge: Intel Corporation Tiger Lake-LP PCI Express Root Port #9 (rev 20)
    00:1e.0 Communication controller: Intel Corporation Tiger Lake-LP Serial IO UART Controller #0 (rev 20)
    00:1e.2 Serial bus controller [0c80]: Intel Corporation Device a0aa (rev 20)
    00:1e.3 Serial bus controller [0c80]: Intel Corporation Tiger Lake-LP Serial IO SPI Controller #1 (rev 20)
    00:1f.0 ISA bridge: Intel Corporation Device a087 (rev 20)
    00:1f.3 Multimedia audio controller: Intel Corporation Tiger Lake-LP Smart Sound Technology Audio Controller (rev 20)
    00:1f.5 Serial bus controller [0c80]: Intel Corporation Tiger Lake-LP SPI Controller (rev 20)
    01:00.0 Non-Volatile memory controller: Sandisk Corp WD Blue SN550 NVMe SSD (rev 01)
    ```
