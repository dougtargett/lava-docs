---
title: Puff Chromeboxes
---

`puff` is a board name for x86_64-based Chromeboxes.

The Collabora LAVA lab contains the following `puff` devices:

-   [Acer Chromebox CXI4](https://www.acer.com/us-en/desktops-and-all-in-ones/acer-chromebox/acer-chromebox-cxi4/pdp/DT.Z1MAA.001) (codename `kaisa`)
    -   Label: `TLA, DESKTOP, CHROMEBOX, KAISA, PVT, US, SKU1-2, INTEL CELERON 5205U, 4GB RAM HYNIX, 32GB EMMC SANDISK, WIFI, BT, USED`
    -   See [acer-chromebox-cxi4-puff](https://lava.collabora.dev/scheduler/device_type/acer-chromebox-cxi4-puff) in LAVA
    -   CPU: Intel Celeron CPU 5205U @ 1.90GHz
    -   GPU: Intel UHD Graphics
    -   Arch: x86_64
    -   AUE Date: 2030-06

### Debugging interfaces

`puff` boards have been flashed and tested with [SuzyQ](../../01-debugging_interfaces) cables.

In an Acer Chromebox CXI4, the debug port is the USB-C port above the charger port.

#### Network connectivity

In the lab, these Chromeboxes are connected to the network through a
Techrise USB-Eth adapter (R8152-based, to provide connectivity at bootloader level) and through the onboard ethernet port (controller is a Realtek R8169).

#### Known issues

See [Common issues](../common_issues/).

### Example kernel command line arguments

    console=ttyS0,115200n8 root=/dev/nfs rw nfsroot=10.154.97.199:/srv/nfs/chromebook,tcp,hard ip=dhcp tftpserverip=10.154.97.199

The IP and path of the NFS share are examples and should be adapted to
the test setup.

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `kaisa` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/puff.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.

### Hardware info

Output of `lshw` and `lspci` commands for the specific device types in the Collabora LAVA lab:

-   `acer-chromebox-cxi4-puff`:

    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    H/W path       Device  Class          Description
    =================================================
    /0/0                   memory         1MiB BIOS
    /0/4                   processor      CPU [empty]
    /0/4/6                 memory         32KiB L1 cache
    /0/4/7                 memory         256KiB L2 cache
    /0/4/8                 memory         2MiB L3 cache
    /0/5                   memory         32KiB L1 cache
    /0/1                   memory         4GiB System memory
    /0/1/0                 memory         4GiB SODIMM DDR4 Synchronous 2400 MHz (0.4
    /0/2                   processor      Intel(R) Celeron(R) CPU 5205U @ 1.90GHz
    /0/100                 bridge         Intel Corporation
    /0/100/2               display        Intel Corporation
    /0/100/14.2            memory         RAM memory
    /0/100/14.3            network        Comet Lake PCH-LP CNVi WiFi
    /0/100/1c              bridge         Intel Corporation
    /0/100/1c/0    eth0    network        RTL8111/8168/8411 PCI Express Gigabit Ethe
    /0/100/1f              bridge         Intel Corporation
    /1             eth1    network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Intel Corporation Device 9b71
    00:02.0 VGA compatible controller: Intel Corporation Device 9baa (rev 04)
    00:04.0 Signal processing controller: Intel Corporation Xeon E3-1200 v5/E3-1500 v5/6th Gen Core Processor Thermal Subsystem
    00:08.0 System peripheral: Intel Corporation Xeon E3-1200 v5/v6 / E3-1500 v5 / 6th/7th/8th Gen Core Processor Gaussian Mixture Model
    00:12.0 Signal processing controller: Intel Corporation Comet Lake Thermal Subsytem
    00:14.0 USB controller: Intel Corporation Comet Lake PCH-LP USB 3.1 xHCI Host Controller
    00:14.2 RAM memory: Intel Corporation Comet Lake PCH-LP Shared SRAM
    00:14.3 Network controller: Intel Corporation Comet Lake PCH-LP CNVi WiFi
    00:14.5 SD Host controller: Intel Corporation Comet Lake PCH-LP SCS3
    00:15.0 Serial bus controller [0c80]: Intel Corporation Serial IO I2C Host Controller
    00:15.2 Serial bus controller [0c80]: Intel Corporation Device 02ea
    00:15.3 Serial bus controller [0c80]: Intel Corporation Device 02eb
    00:19.0 Serial bus controller [0c80]: Intel Corporation Comet Lake Serial IO I2C Host Controller
    00:1a.0 SD Host controller: Intel Corporation Device 02c4
    00:1c.0 PCI bridge: Intel Corporation Device 02be (rev f0)
    00:1e.0 Communication controller: Intel Corporation Device 02a8
    00:1e.2 Serial bus controller [0c80]: Intel Corporation Device 02aa
    00:1f.0 ISA bridge: Intel Corporation Device 0285
    00:1f.3 Multimedia audio controller: Intel Corporation Comet Lake PCH-LP cAVS
    00:1f.4 SMBus: Intel Corporation Comet Lake PCH-LP SMBus Host Controller
    00:1f.5 Serial bus controller [0c80]: Intel Corporation Comet Lake SPI (flash) Controller
    01:00.0 Ethernet controller: Realtek Semiconductor Co., Ltd. RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller (rev 15)
    ```
