---
title: Hana Chromebooks
---

`hana` is a board name for arm64 Chromebooks based on the Mediatek MT8173 SoC.

The Collabora LAVA lab contains the following `hana` devices:

-   Lenovo 300e (codename `hana`)
    -   See [mt8173-elm-hana](https://lava.collabora.dev/scheduler/device_type/mt8173-elm-hana) in LAVA
    -   CPU: 4x Cortex-A72 processors operating at up to 2.0GHz, 4x Arm Cortex-A53 cores
    -   GPU: IMG PowerVR GX6250
    -   SoC: Mediatek MT8173
    -   Arch: Aarch64
    -   AUE Date: 2025-06

Full SoC specs: <https://www.mediatek.com/products/tablets/mt8173>

Mainline Linux kernel support was added in release v5.8-rc1: <https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/arch/arm64/boot/dts/mediatek/mt8173-elm-hana.dts?h=v5.8-rc1>

### Debugging interfaces

`hana` boards have been flashed and tested with [ServoMicro](../../01-debugging_interfaces).

#### Network connectivity

In the lab, these Chromebooks are connected to the network through a
Techrise USB-Eth adapter (R8152-based).

#### Known issues

See [Common issues](../common_issues/).

### Example kernel command line arguments

    console=ttyS0,115200n8 root=/dev/nfs rw nfsroot=10.154.97.199:/srv/nfs/chromebook,tcp,hard ip=dhcp tftpserverip=10.154.97.199

The IP and path of the NFS share are examples and should be adapted to
the test setup.

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `hana` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/hana.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.
