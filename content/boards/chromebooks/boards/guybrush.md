---
title: Guybrush Chromebooks
---

`guybrush` is a board name for x86_64-based Chromebooks.

The Collabora LAVA lab contains the following `guybrush` devices:

-   [Asus Chromebook Spin 514 CP514-3WH](https://www.acer.com/us-en/chromebooks/acer-chromebook-spin-514-cp514-3h-cp514-3hh-cp514-3wh) (codename `dewatt`)
    -   Label: `TLA, NOTEBOOK, CHROMEBOOK, DEWATT, DVT, US, SKU2, AMD RYZEN 3 5400U, 8GB RAM, 64GB, TOUCH, 14IN, FHD, WIFI, BT, USED`
    -   See [acer-cp514-3wh-r0qs-guybrush](https://lava.collabora.dev/scheduler/device_type/acer-cp514-3wh-r0qs-guybrush) in LAVA
    -   CPU: AMD Ryzen 3 5425C
    -   GPU: AMD Radeon 15E7
    -   Arch: x86_64
    -   AUE Date: 2032-06

### Debugging interfaces

`guybrush` boards have been flashed and tested with [SuzyQ](../../01-debugging_interfaces) cables.

In an Asus Chromebook Spin 514, the debug port is the USB-C port on the left side.

`guybrush` requires a fairly recent version of [hdctools](https://chromium.googlesource.com/chromiumos/third_party/hdctools/) for debugging, as the servod overlay for this board was added in January 2021 (see: https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/d3ca5532183eb38a06a1abda978f3694ff5a82f6 ).

In the Collabora lab, we use [docker-servod-tools](https://gitlab.collabora.com/lava/docker-servod-tools) to handle `guybrush` with the required servod version.

#### Network connectivity

In the lab, these Chromebooks are connected to the network through a
Techrise USB-Eth adapter (R8152-based).

#### Known issues

See [Common issues](../common_issues/).

### Example kernel command line arguments

    console=ttyS0,115200n8 root=/dev/nfs rw nfsroot=10.154.97.199:/srv/nfs/chromebook,tcp,hard ip=dhcp tftpserverip=10.154.97.199

The IP and path of the NFS share are examples and should be adapted to
the test setup.

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `dewatt` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/guybrush.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.

### Hardware info

Output of `lshw` and `lspci` commands for the specific device types in the Collabora LAVA lab:

-   `acer-cp514-3wh-r0qs-guybrush`:

    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    H/W path            Device  Class          Description
    ======================================================
    /0/0                        memory         1MiB BIOS
    /0/4                        processor      AMD Ryzen 3 5425C with Radeon Graphic
    /0/4/6                      memory         128KiB L1 cache
    /0/4/7                      memory         2MiB L2 cache
    /0/4/8                      memory         8MiB L3 cache
    /0/5                        memory         128KiB L1 cache
    /0/a                        memory         8GiB System Memory
    /0/a/0                      memory         4GiB LPDDR4 Synchronous 4266 MHz (0.2
    /0/a/1                      memory         4GiB LPDDR4 Synchronous 4266 MHz (0.2
    /0/100                      bridge         Renoir Root Complex
    /0/100/2.1                  bridge         Renoir PCIe GPP Bridge
    /0/100/2.1/0                network        Realtek Semiconductor Co., Ltd.
    /0/100/2.4                  bridge         Renoir PCIe GPP Bridge
    /0/100/8.1                  bridge         Renoir Internal PCIe GPP Bridge to Bu
    /0/100/8.1/0                display        Advanced Micro Devices, Inc. [AMD/ATI
    /0/100/14.3                 bridge         FCH LPC Bridge
    /0/101                      bridge         Renoir PCIe Dummy Host Bridge
    /0/102                      bridge         Renoir PCIe Dummy Host Bridge
    /0/103                      bridge         Renoir PCIe Dummy Host Bridge
    /0/104                      bridge         Advanced Micro Devices, Inc. [AMD]
    /0/105                      bridge         Advanced Micro Devices, Inc. [AMD]
    /0/106                      bridge         Advanced Micro Devices, Inc. [AMD]
    /0/107                      bridge         Advanced Micro Devices, Inc. [AMD]
    /0/108                      bridge         Advanced Micro Devices, Inc. [AMD]
    /0/109                      bridge         Advanced Micro Devices, Inc. [AMD]
    /0/10a                      bridge         Advanced Micro Devices, Inc. [AMD]
    /0/10b                      bridge         Advanced Micro Devices, Inc. [AMD]
    /1                  eth0    network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Renoir Root Complex
    00:00.2 IOMMU: Advanced Micro Devices, Inc. [AMD] Renoir IOMMU
    00:01.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Renoir PCIe Dummy Host Bridge
    00:02.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Renoir PCIe Dummy Host Bridge
    00:02.1 PCI bridge: Advanced Micro Devices, Inc. [AMD] Renoir PCIe GPP Bridge
    00:02.4 PCI bridge: Advanced Micro Devices, Inc. [AMD] Renoir PCIe GPP Bridge
    00:08.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Renoir PCIe Dummy Host Bridge
    00:08.1 PCI bridge: Advanced Micro Devices, Inc. [AMD] Renoir Internal PCIe GPP Bridge to Bus
    00:14.0 SMBus: Advanced Micro Devices, Inc. [AMD] FCH SMBus Controller (rev 51)
    00:14.3 ISA bridge: Advanced Micro Devices, Inc. [AMD] FCH LPC Bridge (rev 51)
    00:18.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Device 166a
    00:18.1 Host bridge: Advanced Micro Devices, Inc. [AMD] Device 166b
    00:18.2 Host bridge: Advanced Micro Devices, Inc. [AMD] Device 166c
    00:18.3 Host bridge: Advanced Micro Devices, Inc. [AMD] Device 166d
    00:18.4 Host bridge: Advanced Micro Devices, Inc. [AMD] Device 166e
    00:18.5 Host bridge: Advanced Micro Devices, Inc. [AMD] Device 166f
    00:18.6 Host bridge: Advanced Micro Devices, Inc. [AMD] Device 1670
    00:18.7 Host bridge: Advanced Micro Devices, Inc. [AMD] Device 1671
    01:00.0 Network controller: Realtek Semiconductor Co., Ltd. Device 8852
    02:00.0 Non-Volatile memory controller: O2 Micro, Inc. Device 8760 (rev 01)
    03:00.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Device 15e7 (rev ea)
    03:00.1 Audio device: Advanced Micro Devices, Inc. [AMD/ATI] Device 1637
    03:00.2 Encryption controller: Advanced Micro Devices, Inc. [AMD] Family 17h (Models 10h-1fh) Platform Security Processor
    03:00.3 USB controller: Advanced Micro Devices, Inc. [AMD] Renoir USB 3.1
    03:00.4 USB controller: Advanced Micro Devices, Inc. [AMD] Renoir USB 3.1
    03:00.5 Multimedia controller: Advanced Micro Devices, Inc. [AMD] Raven/Raven2/FireFlight/Renoir Audio Processor (rev 01)
    03:00.7 Signal processing controller: Advanced Micro Devices, Inc. [AMD] Raven/Raven2/Renoir Sensor Fusion Hub
    ```
