---
title: Cherry Chromebooks
---

`cherry` is a board name for arm64 Chromebooks based on the Mediatek MT8195 SoC.

The Collabora LAVA lab contains the following `cherry` devices:

-   [ACER Chromebook Spin 513 (CP513-2H)](https://www.acer.com/us-en/chromebooks/acer-chromebook-spin-513-cp513-2h/pdp/NX.K0LAA.001) (codename `tomato`)
    -   Label: `CHROMEBOOK, TOMATO, EVT, US, SKU2, MTK 8195, 4GB RAM, 64GB, 13.5IN, QHD, WIFI, BT`
    -   See [mt8195-cherry-tomato-r2](https://lava.collabora.dev/scheduler/device_type/mt8195-cherry-tomato-r2) in LAVA
    -   CPU: 4x ARM Cortex-A78 @ 2.2 GHz, 4x ARM Cortex-A55 @ 2.0 GHz
    -   GPU: Arm Mali-G57
    -   SoC: Mediatek Kompanio 1200 (MT8195)
    -   Arch: Aarch64
    -   AUE Date: 2032-06

Full SoC specs: <https://www.mediatek.com/products/chromebooks/mediatek-kompanio-1200>

Mainline Linux kernel support was added in release v6.0-rc1: <https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/arch/arm64/boot/dts/mediatek/mt8195-cherry-tomato-r2.dts?h=v6.0-rc1>

### Debugging interfaces

`cherry` boards have been flashed and tested with [SuzyQ](../../01-debugging_interfaces) cables.

In the ACER Chromebook Spin 513, the debug port is the USB-C port on the left side.

`cherry` requires a fairly recent version of [hdctools](https://chromium.googlesource.com/chromiumos/third_party/hdctools/) for debugging, as the servod overlay for this board was added in March 2021 (see: https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/1f2602943ac951b062f1b118baa5eaa11b62b633 ).

In the Collabora lab, we use [docker-servod-tools](https://gitlab.collabora.com/lava/docker-servod-tools) to handle `cherry` with the required servod version.

#### Network connectivity

In the lab, these Chromebooks are connected to the network through an Asix AX88772B AX88772 USB-Eth adapter.

#### Known issues

See [Common issues](../common_issues/).

### Example kernel command line arguments

    console=ttyS0,115200n8 root=/dev/nfs rw nfsroot=10.154.97.199:/srv/nfs/chromebook,tcp,hard ip=dhcp tftpserverip=10.154.97.199

The IP and path of the NFS share are examples and should be adapted to
the test setup.

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `tomato` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/cherry.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.
