---
title: Dedede Chromebooks
---

`dedede` is a board name for x86_64-based Chromebooks. Many vendors make Chromebooks based on this board, some examples:

-   [HP Chromebook 14 G7](https://support.hp.com/us-en/document/c07056426)
-   [Lenovo 500e Chromebook Gen 3](https://www.lenovo.com/us/en/coming-soon/500e-G3/p/22ED05E5EN3)
-   [Asus Chromebook CX1500](https://www.asus.com/Laptops/For-Home/Chromebook/ASUS-Chromebook-CX1-CX1500/)
-   [Galaxy Chromebook Go](https://www.samsungmobilepress.com/mediaresources/galaxy_chromebook_go)
-   [Acer CB317-1H](https://www.acer.com/ac/en/US/content/series/acerchromebook317)

These chromebooks use 64 bit Intel Jasper Lake processors like the Intel
Celeron N4500. The rest of the specs may vary between
vendors and models.

The Collabora LAVA lab contains the following `dedede` devices:

-   [Acer CB317-1H](https://www.acer.com/us-en/chromebooks/acer-chromebook-317-cb317-1h-cb317-1ht/) (codename `magpie`)
    -   Label: `MAGPIE, PVT, US, SKU1, INTEL CELERON N4500, 4GB RAM, 64GB EMMC, 17.3IN, FHD, WIFI, BT`
    -   See [acer-cb317-1h-c3z6-dedede](https://lava.collabora.dev/scheduler/device_type/acer-cb317-1h-c3z6-dedede) in LAVA
    -   CPU: Intel Celeron N4500 @ 1.10GHz
    -   GPU: JasperLake [UHD Graphics]
    -   Arch: x86_64
    -   AUE Date: 2031-06

### Debugging interfaces

`dedede` boards have been flashed and tested with [SuzyQ](../../01-debugging_interfaces) cables.

On the Acer CB317-1H, the debug port is the USB-C port on the left side.

#### Network connectivity

In the lab, these Chromebooks are connected to the network through a
Techrise USB-Eth adapter (R8152-based).

#### Known issues

See [Common issues](../common_issues/).

### Example kernel command line arguments

    console=ttyS0,115200n8 root=/dev/nfs rw nfsroot=10.154.97.199:/srv/nfs/chromebook,tcp,hard ip=dhcp tftpserverip=10.154.97.199

The IP and path of the NFS share are examples and should be adapted to
the test setup.

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `magolor` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/dedede.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.

### Hardware info

Output of `lshw` and `lspci` commands for the specific device types in the Collabora LAVA lab:

-   `acer-cb317-1h-c3z6-dedede`:

    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    H/W path       Device  Class          Description
    =================================================
    /0/0                   memory         1MiB BIOS
    /0/4                   processor      Intel(R) Celeron(R) N4500 @ 1.10GHz
    /0/4/6                 memory         64KiB L1 cache
    /0/4/8                 memory         4MiB L3 cache
    /0/5                   memory         64KiB L1 cache
    /0/a                   memory         4GiB System Memory
    /0/a/0                 memory         4GiB LPDDR4 Synchronous 2933 MHz (0.3 ns)
    /0/100                 bridge         Intel Corporation
    /0/100/2               display        Intel Corporation
    /0/100/14.2            memory         RAM memory
    /0/100/14.3            network        Intel Corporation
    /0/100/1f              bridge         Intel Corporation
    /1             eth0    network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Intel Corporation Device 4e22
    00:02.0 VGA compatible controller: Intel Corporation Device 4e55 (rev 01)
    00:04.0 Signal processing controller: Intel Corporation Device 4e03
    00:05.0 Multimedia controller: Intel Corporation Device 4e19
    00:08.0 System peripheral: Intel Corporation Device 4e11
    00:14.0 USB controller: Intel Corporation Device 4ded (rev 01)
    00:14.2 RAM memory: Intel Corporation Device 4def (rev 01)
    00:14.3 Network controller: Intel Corporation Device 4df0 (rev 01)
    00:14.5 SD Host controller: Intel Corporation Device 4df8 (rev 01)
    00:15.0 Serial bus controller [0c80]: Intel Corporation Device 4de8 (rev 01)
    00:15.1 Serial bus controller [0c80]: Intel Corporation Device 4de9 (rev 01)
    00:15.2 Serial bus controller [0c80]: Intel Corporation Device 4dea (rev 01)
    00:15.3 Serial bus controller [0c80]: Intel Corporation Device 4deb (rev 01)
    00:19.0 Serial bus controller [0c80]: Intel Corporation Device 4dc5 (rev 01)
    00:19.2 Communication controller: Intel Corporation Device 4dc7 (rev 01)
    00:1a.0 SD Host controller: Intel Corporation Device 4dc4 (rev 01)
    00:1e.0 Communication controller: Intel Corporation Device 4da8 (rev 01)
    00:1e.2 Serial bus controller [0c80]: Intel Corporation Device 4daa (rev 01)
    00:1f.0 ISA bridge: Intel Corporation Device 4d87 (rev 01)
    00:1f.3 Multimedia audio controller: Intel Corporation Device 4dc8 (rev 01)
    00:1f.5 Serial bus controller [0c80]: Intel Corporation Device 4da4 (rev 01)
    ```
