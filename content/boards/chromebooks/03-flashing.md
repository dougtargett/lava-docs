---
title: Flashing firmware on Chromebooks
weight: 3
---

Each Chromebook model in the lab uses a different type of firmware, but
they all have some commonalities.  They are all based on Coreboot with a
custom build of Depthcharge to enable serial console and run the
tftpboot command interactively.  They can all be flashed using a special
version of `flashrom` which is built with the Chromium OS SDK.  This
page explains how to flash firmware images on all the Chromebook models
present in the Collabora lab.


## Prerequisites

### Install servod packages

First of all, the `servod` tools needs to be installed as explained in
the [Chromebooks and Servo boards](../01-debugging_interfaces)
documentation.  Typically, all the LAVA rack dispatchers in the lab will
have these packages automatically installed by Chef.


### Install the firmware tools

To install the tools needed to flash the Chromebook firmware images
follow [these
instructions](https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/README.md#installing)
from the firmware tools README. This can be run on a dispatcher itself
in the lab, to be able to then flash Chromebook devices directly.


## Flashing a firmware image

Each Chromebook type requires a different firmware image to be flashed.
They are all currently all stored in
[images.collabora.co.uk](https://images.collabora.co.uk/lava/boot) and
need to be downloaded manually.

See the general [flashing
instructions](https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/README.md#flashing)
from the firmware tools README, and the specific ones for the Collabora
test lab below.

**NOTE:** Before flashing a Chromebook for the first time, always do a
backup first with `servoflash.py --backup`.

For example, to flash the firmware for a `rk3399-gru-kevin` device:

* SSH to the dispatcher where the device is attached and enter
  firmware-tools

  ```
  ssh lava-rack-cbg-2
  cd firmware-tools
  ```

* Download the firmware file. For `rk3399-gru-kevin` it's:

  ```
  wget https://images.collabora.co.uk/lava/boot/rk3399-gru-kevin/depthcharge-rk3399-gru-kevin-20180806.dev.bin
  ```
  
  For chromebooks it may be in the `chromium/firmware-tools` repo.
  When flashing a development or WIP firmware, make sure to checkout the correct branch
  and checkout the file using:

  ```
  git lfs fetch
  git lfs checkout
  ```

* Ensure the device is off before flashing

  ```
  docker exec <board-hostname>-docker_servod dut-control cold_reset:on
  ```

* Run the `servoflash.py` script with the names of the device to flash
  and the matching firmware file:

  ```
  ./servoflash.py \
    --device=rk3399-gru-kevin-cbg-0 \
    --firmware=depthcharge-rk3399-gru-kevin-20180806.dev.bin
  ```

It can take a few minutes.  There should be these messages around the
end, which can vary depending on the type of Chromebook:

```
Erasing and writing flash chip... Verifying flash... VERIFIED.
SUCCESS
```

Set the device status to "unknown" via the LAVA web interface or
`lavacli` and check that the healthcheck passes.

### Flashing the EC firmware

The `servoflash.py` script mentioned above flashes the firmware on the AP (i.e. the Application Processor) only. Normally, reflashing the AP firmware is enough for the purpose of enabling a Chromebook in the Collabora LAVA lab; every now and then there might be a need to reflash the EC (i.e. the Embedded Controller) firmware as well (e.g. to test features/fixes included in newer versions). 

The `flash_ec` script, part of the [ChromiumOS EC utils](https://chromium.googlesource.com/chromiumos/platform/ec/+/refs/heads/main/util/flash_ec), can be used to program the EC firmware on a Chromebook when needed.

Some EC chips can be programmed using the custom `flashrom` binary contained in the Chromium OS SDK, others require different utilities based on the communication protocol used (I2C, SPI, UART, etc.). 

The `flash_ec` script detects the Chromebook's EC chip and invokes the proper utility to flash it. The script can be executed from within the Chromium OS development chroot, as explained in the [ChromiumOS EC flashing documentation](https://chromium.googlesource.com/chromiumos/platform/ec/+/HEAD/README.md#flashing-an-ec-binary-to-a-board).

The `flash_ec` script can also be used outside the CrOS chroot, using the hdctools Docker container. **Note**: The EC build folder that includes the EC binaries and the respective monitor binary needs to be mounted in the docker container:

* Start a servod docker container and mount the EC build folder. Instructions for setting up the container can be found [here](https://docs.google.com/document/d/e/2PACX-1vRGZ8yAfwzp6vlLZVGpJYQIFdv7_gR7yt6F6_Afk_2gWBlun5p-juZvOuHia9vfcOK88f4d6lIR1HqZ/pub) (the EC build folder can be added to the `volumes` list).

* Enter the servod docker container, move to the folder with the EC binaries and and run the `flash_ec` script:
  ```
  root@docker_servod:/tmp/ec/# flash_ec --image <path-to-ec.bin> --board <board>
  ```

**NOTE:** The EC firmware binary can be built from inside the CrOS chroot, as explained in the [ChromiumOS EC documentation](https://chromium.googlesource.com/chromiumos/platform/ec/+/HEAD/README.md#building-an-ec-binary). A prebuilt EC firmware binary can also be found inside the stock Google release archives.

The chip version of a Chromebook's EC can be detected using the following command:

```
docker exec <board-hostname>-docker_servo dut-control ec_chip
```

## Troubleshooting

 * If the cr50 console is boot looping or failing verification:
   1. Try powering on the device using the `Esc + Refresh + Power` button combo
   2. Try powering the machine on and off both with and without cables attached
   3. Try swapping SuzyQ cables
   4. Try reflashing
   5. Try recharging the battery. You can check the battery by running `battery` in the ec console. The ec console device can be found by running `dut-control -p PORT ec_uart_pty`.
