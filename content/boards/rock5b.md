
---
title: ROCK 5B RK3588
---

Note: A lot of the information here was taken from [Collabora's Rock5b notes
repository][2].

ROCK 5B is a Rockchip RK3588 based SBC (Single Board Computer) by Radxa.

There are tree variants depending on the DRAM size : 4G, 8G and 16G.

Specification:
* Rockchip Rk3588 SoC
* 4x ARM Cortex-A76, 4x ARM Cortex-A55
* 4/8/16GB memory LPDDR4x
* Mali G610MC4 GPU
* MIPI CSI 2 multiple lanes connector
* eMMC module connector
* uSD slot (up to 128GB)
* 2x USB 2.0, 2x USB 3.0
* 2x HDMI output, 1x HDMI input
* Ethernet port
* 40-pin IO header including UART, SPI, I2C and 5V DC power in
* USB PD over USB Type-C
* Size: 85mm x 54mm

This board is available from [Radxa distributors](https://wiki.radxa.com/Buy).

### Power control

The board can be powered on with 5V via pins 2 or 4 in the GPIO header (as shown
in the [GPIO pinout][1]), and GND on pin 6 for example. The pins are colored
accordingly on the latest board revision:

![Power through GPIO](/img/rock5b-power-gpio.jpg)

Alternatively, the board can be powered via a USB PD Type-C connector:
* 9V/2A, 12V/2A, 15V/2A, 20V/2A

Radxa have tested various power supplies, making a comprehensive list of
supplies which work and those which do not work [here][3],

### Low-level boot control

Serial connection on the board can be done through pins on the GPIO. The board's
TX is on pin 8 and RX on pin 10 as per [the pinout][1]. GND can be connected
on pin 6 for example. Serial specification:
* 3.3V TTL
* 1500000 8n1

![Serial through GPIO](/img/rock5b-serial-gpio.jpg)

#### Network connectivity

A USB to Ethernet dongle is required for network connectivity. This has to
be plugged into an USB type A port, and the network cable connected to the
dongle.

### Bootloader

For the bootloader setup, a ready-to-use image is available at [Collabora's
Rock5B U-boot repository][4] as an artifact from the CI. After flashing and
dropping to the u-boot prompt, tftp and nfs can be configured to retrieve the
kernel and rootfs.

The main bootloader is stored in SPI flash. Instructions on how to flash
the memory are available [here][5].

### Lab notes and trouble shooting

None.

[1]: https://wiki.radxa.com/Rock5/hardware/5b/gpio
[2]: https://gitlab.collabora.com/hardware-enablement/rockchip-3588/notes-for-rockchip-3588
[3]: https://wiki.radxa.com/Rock5/5b/power_supply
[4]: https://gitlab.collabora.com/hardware-enablement/rockchip-3588/u-boot
[5]: https://gitlab.collabora.com/hardware-enablement/rockchip-3588/notes-for-rockchip-3588/-/blob/main/flash_bootloader_spi.md
