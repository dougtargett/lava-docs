---
title: UP Squared 6000 board
---

[UP Squared 6000](https://up-board.org/up-squared-6000/) series board based on
the latest [Intel Atom x6425RE](https://ark.intel.com/content/www/us/en/ark/products/207899/intel-atom-x6425re-processor-1-5m-cache-1-90-ghz.html)
processors.

UP-board provides the [UP Squared 6000 series](https://up-shop.org/ups6000series.html),
with different memory, processor and eMMC/storage options.

![UP Squared 6000 AT 08/64 board](/img/upsquared6000_board.jpg)

Specifications: [UPN-EHLX4RE-A10-0864](https://up-shop.org/ups6000series.html):

* Model name: UP Squared 6000 AT 08/64
* SKU: UPN-EHLX4RE-A10-0864
* Processor: Intel Atom x6425RE
  * 4x cores @ 1.9GHz
* Graphics:
  * Intel Graphics GEN 10
* Memory: 8GB
* Storage:
  * eMMC: 64 GB
* Display:
  * 1x HDMI 2.0b
  * 1x DP 1.2
  * 1x MIPI DSI/eDP (4K@60hz)
* USB:
  * 2x USB 3.2 Gen2 Type A
  * 1xUSB 3.2 Gen2 Type C (OTG support)
* UART:
  * via. USB/UART pin header [cable](https://up-shop.org/usb-2-0-pin-header-cable.html)
* Ethernet:
  * 1x 1 GbE RJ-45 (Intel I211-AT)
  * 1x 1/2.5 GbE RJ-45 (Intel I225-V, support TSN)
* WiFi/Bluetooth:
  * via. M.2 2230 [AX210.NGWG.NV module](https://www.mouser.in/ProductDetail/Intel/AX210.NGWG.NV?qs=DPoM0jnrROU0HT87ANhu5g%3D%3D)

## Power control

Board can be powered via a DC Power Jack 12V/6A DC input, center positive. Note
that the DC Power Jack on the board is not standard, so the following items
could be bought:

* Power supply 12V: https://up-shop.org/12v-6a-72w-psu.html
* Power cord (different types of plugs are available): https://up-shop.org/power-cord-u-s-plug.html

## Serial console

To configure the serial console, follow the instructions from the
[UP Squared 6000 setup](https://www.apertis.org/reference_hardware/upsquared6000_setup/#serial-console).

## Network connectivity

There is a standard Gigabit Ethernet RJ-45 Jack on the device that can be used.

## Boot selection

Use the standard UEFI firmware provided with the UP Squared 6000 boards, with
settings reset to the factory defaults.

The Network interface needs to be enabled and boot order set to try PXE
IPv4/IPv6 first, to make LAVA take the priority over previously installed images
on the device. The following steps needs to be configured:

* Enter BIOS setup pressing `<DEL>` or `<ESC>` during boot.
* Go to tab `Advanced` -> `Network Stack Configuration`.
* Enable option `Network Stack`.
* Go to tab `Boot` -> `Boot Option #1`.
* Select option `Network:UEFI: PXE` for the IPv4/IPv6 variant you use.
* Press F4 to save and Y to confirm.
* Exit the menu and restart the system.

This should then automatically try PXE boot and launch GRUB.

## Bootloader

For the bootloader the [LAVA GRUB build](https://gitlab.collabora.com/lava/grub/)
should be used (as it is built with the required version and modules for
booting this board).

The GRUB EFI bootloader can be served on a TFTP server and run on PXE boot.
Documentation on how to build/install this can be found on the
[apertis setup page](https://www.apertis.org/reference_hardware/upsquared6000_setup_lava/)

### Health checks

* Mainline Linux kernel support in AMD64 defconfig since release v5.15.
* Apertis Linux ARM64 package support since release 5.15.34-0+apertis1.

### Lab notes and trouble shooting

None.
