---
title: lava-rack-cbg-A
---

# tumbleweed
Controller is [Intel NUC - NUC6CAYB](https://www.intel.com/content/dam/support/us/en/documents/boardsandkits/NUC6CAYB_TechProdSpec.pdf). 
The server can be accessed over SSH and has the IP address of 192.168.101.242. 
There are 2 [16 port switches - GSS116E](https://www.netgear.com/images/datasheet/switches/clickswitches/GSS108E_GSS108EPP_GSS116E_DS.pdf) connected Tumbleweed. 
The USB hub is a [Manhatten MondoHub II](https://manhattanproducts.eu/products/manhattan-en-mondohub-ii-163606).


# Devices from top to bottom

[rk3288-veyron-jaq-cbg-1](https://lava.collabora.co.uk/scheduler/device/rk3288-veyron-jaq-cbg-1) 
power connected to the [vertical power bar](https://www.racksolutions.co.uk/vertical-rack-mount-power-strips-8.html), 
connection to USB hub using a [ServoV2](https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/HEAD/docs/servo_v2.md), 
there are 2 connections to the ServoV2 from the DUT, 
one using a USB enabling the hub for the ethernet connection and one using a [Yoshi Flex Cable](https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/HEAD/docs/servo_v2.md#yoshi-flex-cable) 
enabling the consoles. Connection to the switch is by ethernet cable from the ServoV2 to the switch.

[rk3288-veyron-jaq-cbg-0](https://lava.collabora.co.uk/scheduler/device/rk3288-veyron-jaq-cbg-0) 
power connected to the vertical power bar, 
connection to USB hub using a ServoV2, 
there are 2 connections to the ServoV2 from the DUT, 
one using a USB enabling the hub for the ethernet connection and one using a Yoshi Flex Cable enabling the consoles. 
Connection to the switch is by ethernet cable from the ServoV2 to the switch.

[tegra124-nyan-big-cbg-0](https://lava.collabora.co.uk/scheduler/device/tegra124-nyan-big-cbg-0) 
power connected to the vertical power bar, 
connection to USB hub using a ServoV2, 
there are 2 connections to the ServoV2 from the DUT, 
one using a USB enabling the hub for the ethernet connection and one using a Yoshi Flex Cable enabling the consoles. 
Connection to the switch is by ethernet cable from the ServoV2 to the switch.

2x 16 port switches power cables connected to the vertical power bar. 
One switch connected to the core switch with the other switch linked off.

[pms-1](https://energenie4u.co.uk/res/pdfs/ENER019_User_Manual.pdf) 
configuration for the IP address is in the [chef repository](https://gitlab.collabora.com/lava/collabora-lava-setup/-/blob/master/chef/cookbooks/lava/files/host-baobab.collabora.co.uk/dhcp-hosts.conf), 
provides power switching for the devices on the same shelf which are:  
[meson-g12b-a311d-khadas-vim3-cbg-0](https://lava.collabora.co.uk/scheduler/device/meson-g12b-a311d-khadas-vim3-cbg-0) 
Power connected to pms-1, 
uart console connected to USB hub using a [USB to UART 3 way female pin headers cable](https://ftdichip.com/products/ttl-232r-rpi/), 
network connected using a ethernet cable to the switch.  
[minnowboard-turbot-E3826-cbg-0](https://lava.collabora.co.uk/scheduler/device/minnowboard-turbot-E3826-cbg-0) 
Power connected to pms-1, 
uart console connected to the USB hub using a [USB to UART 6pin SIL](https://ftdichip.com/products/ttl-232r-3v3/), 
network connected using a ethernet cable to the switch.  
[minnowboard-turbot-E3826-cbg-1](https://lava.collabora.co.uk/scheduler/device/minnowboard-turbot-E3826-cbg-1) 
Power connected to pms-1, 
uart console connected to the USB hub using a 'USB to UART 6pin SIL', 
network connected via ethernet cable to the switch.  
[meson-g12b-a311d-khadas-vim3-cbg-2](https://lava.collabora.co.uk/scheduler/device/meson-g12b-a311d-khadas-vim3-cbg-2) 
Power connected to pms-1, 
uart console connected to USB hub using a 'USB to UART 3 way female pin headers cable', 
network connected using a ethernet cable to the switch.

[pms-0](https://www.aviosys.com/products/9850.php) 
configuration for the IP address is in the chef repository, 
provides power switching for the devices on the same shelf which are:  
[bcm2836-rpi-2-b-cbg-0](https://lava.collabora.co.uk/scheduler/device/bcm2836-rpi-2-b-cbg-0) 
Power connected to pms-0, 
uart console connected to USB hub using a 'USB to UART 3 way female pin headers cable', 
network connected using a ethernet cable to the switch.  
[rk3288-rock2-square-cbg-0](https://lava.collabora.co.uk/scheduler/device/rk3288-rock2-square-cbg-0) 
Power connected to pms-0, 
uart console connected to USB hub using a 'USB to UART 3 way pin headers cable', 
network connected using a ethernet cable to the switch.  
[meson-g12b-a311d-khadas-vim3-cbg-1](https://lava.collabora.co.uk/scheduler/device/meson-g12b-a311d-khadas-vim3-cbg-1) 
Power connected pms-0, 
uart console connected to USB hub using a 'USB to UART 3 way female pin headers cable', 
network connected using a ethernet cable to the switch.  

Server connected to the [horizontal power bar](https://www.racksolutions.co.uk/catalog/product/view/id/2722/s/8-way-13-amp-horizontal-rack-mount-power-strips/), 
USB connection to the USB hub and network connection to the switch. 
Configured using [chef](https://gitlab.collabora.com/lava/collabora-lava-setup). 
USB hub connected through a hard wired usb to the Disptcer Type-A and to the horizontal power bar, 
then to each of the devices.

[eth008-1](http://www.robot-electronics.co.uk/files/eth008.pdf) 
configuration for the IP address is in the chef repository, 
provides power switching for the devices on the same shelf which are:  
[exynos5422-odroidxu3-cbg-0](https://lava.collabora.co.uk/scheduler/device/exynos5422-odroidxu3-cbg-0) 
Power connected to the horizontal power bar going through eth008-1, 
console connected using a USB Micro cable and [USB to Serial UART Bridge](https://lilliputdirect.com/odroid-usb-uart), 
network connected using an ethernet cable to the switch.  
[sun50i-h6-pine-h64-cbg-0](https://lava.collabora.co.uk/scheduler/device/sun50i-h6-pine-h64-cbg-0) 
Power connected to the horizintal power bar going through eth008-1, 
console connected using a USB uo UART 3 way female pin headers cable, 
network connected using an ethernet cable to the switch.  
[sun50i-h6-pine-h64-cbg-1](https://lava.collabora.co.uk/scheduler/device/sun50i-h6-pine-h64-cbg-1) 
Power connected to the horizintal power bar through the eth008-1, 
console connected using a USB to UART 3 way female pin headers cable, 
network connected using an ethernet cable to the switch.  

[5v PSU](https://uk.farnell.com/tracopower/txm-075-105/power-supply-ac-dc-75w-5v-12a/dp/2363868?ost=txm+075-105) 
Power connected to horizontal power bar.  
basil.lava.cbg.collabora.co.uk, provides reset control using GPIO pins for the m3ulcb devices, 
power connected to the 5v PSU, network connected via ethernet cable to the switch.  
[r8a7796-m3ulcb-cbg-0](https://lava.collabora.co.uk/scheduler/device/r8a7796-m3ulcb-cbg-0) 
Power from 5v PSU, reset control from basil, 
console connected to USB hub using a USB type-A to micro USB cable, 
network using an ethernet cable to the switch.  
[r8a7796-m3ulcb-cbg-1](https://lava.collabora.co.uk/scheduler/device/r8a7796-m3ulcb-cbg-1) 
Power from 5v PSU, reset control from basil, 
console connected to USB hub using a USB type-a to micro USB cable, 
network using anuser ethernet cable to the switch.  


# [Orchid](https://staging.lava.collabora.dev/scheduler/worker/orchid)
Dispatcher is [Cyberserve Atom-104i](https://www.broadberry.co.uk/intel-atom-rackmount-servers/cyberserve-atom-104i) 
and the server specification can be found on one of [the quotations from broadberry.](/files/PDAR52995.pdf). 
The server can be accessed over SSH and has the IP address of 10.154.97.199. 
The switch for the rack is a [Zyxel GS1900-24](https://www.zyxel.com/uk/en/products_services/8-10-16-24-48-port-GbE-Smart-Managed-Switch-GS1900-Series/specification) 
and is on IP address 10.154.97.200. 
The USB hub is a [belkin F5U307](https://www.belkin.com/us/support-product?pid=01t80000002G1etAAC)

Switch Power connected to the vertical power bar, 
connection to the core switch, 
connection to the dispatcher and then to each of the devices. 
Configuration found [here](https://gitlab.collabora.com/lava/collabora-lava-setup/-/blob/master/doc/rack-assembly.md#managed-ethenet-switch)

Server connected to the vertical power bar, 
USB connection to the usb hub and network connected to the switch. 

[pms-0](https://energenie4u.co.uk/res/pdfs/ENER011%20UM.pdf) configuration is in the chef repository  
[minnowboard-max-E3825-cbg-0](https://staging.lava.collabora.dev/scheduler/device/minnowboard-max-E3825-cbg-0) 
Power connected to pms-0, 
console connected via 'USB to UART 6pin sil', 
network connected viaethernet cable to the switch.

[hp-x360-12b-ca0500na-n4000-octopus-cbg-0](https://staging.lava.collabora.dev/scheduler/device/hp-x360-12b-ca0500na-n4000-octopus-cbg-0) 
power connected to the vertical power bar, connection to the USB hub via SuzyQable, 
connection to the switch using ethernet cable and TechRise USB Ethernet adapter.

[rk3399-gru-kevin-cbg-5](https://staging.lava.collabora.dev/scheduler/device/rk3399-gru-kevin-cbg-5) 
power connected to the vertical power bar, 
connection to the USB hub using [servoMicro](https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/HEAD/docs/servo_micro.md) 
and a [USB type A to usb Micro cable](https://cpc.farnell.com/pro-signal/psg91471/lead-usb2-0-a-male-micro-b-male/dp/CS30867), 
connection to the switch using a network cable and a TechRise USB Ethernet adapter.

[mt8173-elm-hana-cbg-0](https://staging.lava.collabora.dev/scheduler/device/mt8173-elm-hana-cbg-0) 
power connected to the vertical power bar, 
connection to the USB hub using servoMicro and a USB type A to usb Micro cable, 
connection to the switch using a network cable and a TechRise USB Etheret adapter.

These devices are subject to change depending testing and roll out procedure.
